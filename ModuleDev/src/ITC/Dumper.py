'''
Created on 27.06.2015

@author: wstahl
'''
import pprint

class Dumper(object):
    '''
    Helperclass to dump an object
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.__dumper = pprint.PrettyPrinter(indent=4)
        
    def dump(self, var):
        self.__dumper.pprint(var)
        
    def getDump(self, var):
        return self.__dumper.pformat(var)

