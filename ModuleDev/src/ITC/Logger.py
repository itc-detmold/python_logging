'''
Created on 27.06.2015

@author: wstahl
'''

import logging.handlers

class Logger(object):
    '''
    Logging class
    '''


    def __init__(self, logfilename, level_name="debug", size=10):
        '''
        Initializes Logger object with
        - logfilename = basename of logfile
        - level_name = level switch for logger (defaults to "debug")
        - size = size of one logfile in MB (defaults to 10 MB)
        '''
        self.__MB = 1024 * 1024 # 1 MB in Bytes
        LEVELS = {"debug": logging.DEBUG, "info": logging.INFO, "warning": logging.WARNING, "error": logging.ERROR, "critical": logging.CRITICAL}
        
        self.__logger = logging.getLogger("ITCLogger")
        self.__logger.setLevel(LEVELS.get(level_name, logging.NOTSET))
        
        self.__handler = logging.handlers.RotatingFileHandler(logfilename, maxBytes=(size * self.__MB), backupCount=5)
        self.__logger.addHandler(self.__handler)
        
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)d - %(message)s")
        self.__handler.setFormatter(formatter)
    
    def getLogger(self):
        return self.__logger
    
    def shutdown(self):
        logging.shutdown()  
          
    def isNotEmpty(self, s):
        return bool(s and s.strip())
