'''
Created on 27.06.2015

@author: wstahl
'''

from ITC import Logger
from ITC import Dumper
import glob 

def logmessage(logger):
    mylist = ['Hugo', 'Emil', ['1', '2']]
    dumper = Dumper()
    
    for i in range(200000):
        logger.debug("i = %d" % i + " / myList = " + dumper.getDump(mylist))
        
if __name__ == '__main__':

    LOG_FILENAME = "/Users/wstahl/tmp/logging_example.log"

    # initialize Logging system
    logsystem = Logger(LOG_FILENAME)
    my_logger = logsystem.getLogger()
    
    
    logmessage(my_logger)

    logsystem.shutdown()
    logfiles = glob.glob("%s*" % LOG_FILENAME)
    for filename in logfiles:
        print filename
     
  
    